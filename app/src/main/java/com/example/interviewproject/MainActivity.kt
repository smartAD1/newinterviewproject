package com.example.interviewproject

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import com.example.interviewproject.viewModel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val viewModel: MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnEvent()
        viewModel.run {
            searchBarStatusLiveData.observe(this@MainActivity, {
                search_view.visibility = if (it) View.VISIBLE else View.INVISIBLE
            })
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.fetchData()
    }

    private fun btnEvent() {
        search_view.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                search_view.clearFocus()
                viewModel.searchUsers("$query")
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText?.length == 0) viewModel.fetchData()
                return true
            }
        })
    }
}