package com.example.interviewproject.page

import android.os.Bundle
import android.view.FrameStats
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.interviewproject.R
import com.example.interviewproject.adapter.ItemClick
import com.example.interviewproject.adapter.MainAdapter
import com.example.interviewproject.model.response.GithubResponse
import com.example.interviewproject.model.response.SearchUserResponse
import com.example.interviewproject.model.response.Status
import com.example.interviewproject.utils.SearchBarStatus
import com.example.interviewproject.utils.toasts
import com.example.interviewproject.viewModel.MainViewModel
import kotlinx.android.synthetic.main.fragment_main.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel


class MainFragment : Fragment(), ItemClick {

    private val viewModel: MainViewModel by sharedViewModel()
    private val navController: NavController by lazy { findNavController() }
    private val adapter: MainAdapter by lazy {
        MainAdapter(this)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_main, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        context?.let {
            userListView.layoutManager = LinearLayoutManager(it)
            userListView.adapter = adapter
        }
        bindViewModel()
    }

    private fun bindViewModel() {
        viewModel.run {
            statusLiveData.observe(viewLifecycleOwner, {
                when (it) {
                    is Status.Success<*> -> {
                        val data = it.data as? ArrayList<GithubResponse>?
                        data?.let {
                            setData(data)
                        }
                        SearchBarStatus.searchBarStatus = true
                        setStatusBar()
                    }
                    is Status.Error -> requireActivity().toasts(it.message)
                    is Status.OnFailure -> {
                        requireActivity().toasts("error")
                    }
                }
            })
            searchUserDataStatus.observe(viewLifecycleOwner, {
                when (it) {
                    is Status.Success<*> -> {
                        val mData = it.data as? SearchUserResponse
                        mData?.let {
                            searchUserResult(it)
                        }
                    }
                    is Status.Error, -> requireActivity().toasts(it.message)
                    is Status.OnFailure -> requireActivity().toasts("error")
                }
            })
        }
    }

    private fun MainViewModel.searchUserResult(it: SearchUserResponse) {
            adapter.data.clear()
            adapter.data = it.items
            adapter.notifyDataSetChanged()
    }

    private fun setData(data: ArrayList<GithubResponse>) {
        adapter.data = data
        adapter.notifyDataSetChanged()
        userListView.adapter = adapter
    }


    override fun getCallBack(login: String) {
        val args = MainFragmentDirections.actionMainFragment2ToUserContentFragment(login)
        navController.navigate(args)
        SearchBarStatus.searchBarStatus = false
        viewModel.setStatusBar()
    }
}