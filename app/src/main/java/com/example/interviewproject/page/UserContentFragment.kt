package com.example.interviewproject.page

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.interviewproject.R
import com.example.interviewproject.model.response.Status
import com.example.interviewproject.model.response.UserResponse
import com.example.interviewproject.utils.SearchBarStatus
import com.example.interviewproject.utils.toasts
import com.example.interviewproject.viewModel.UserContentViewModel
import kotlinx.android.synthetic.main.fragment_user_content.*
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class UserContentFragment : Fragment() {

    private var id = ""
    private val viewModel: UserContentViewModel by sharedViewModel()
    private val navController: NavController by lazy { findNavController() }
    private val args: UserContentFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_user_content, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        id = args.id
        fetchRemoteData()
        bindViewModel()
        btnEvent()
    }


    private fun bindViewModel() =
        viewModel.run {
            statusLiveData.observe(viewLifecycleOwner, Observer {
                when (it) {
                    is Status.Success<*> -> {
                        val data = it.data as? UserResponse
                        setData(data)
                    }
                    is Status.Error -> requireActivity().toasts(it.message)
                    is Status.OnFailure ->  requireActivity().toasts("error")
                }
            })
        }

    private fun setData(data: UserResponse?) {
        data?.let { mData ->
            Glide.with(requireContext()).load(mData.avatarUrl)
                .apply(RequestOptions.circleCropTransform()).into(userPicture)
            isUserName.text = mData.name
            bio.text = mData.bio
            locationStr.text = mData.location
            linkStr.text = mData.blog
            avatarName.text = mData.login
            site.text = mData.type
            userpage.postDelayed({
                userpage.visibility = View.VISIBLE
            },400)
        }
    }

    private fun fetchRemoteData() = viewModel.fetchUserData(id)

    private fun btnEvent() {
        close.setOnClickListener {
            navController.popBackStack()
        }
    }
}