package com.example.interviewproject.apiService

import com.example.interviewproject.model.response.GithubResponse
import com.example.interviewproject.model.response.SearchUserResponse
import com.example.interviewproject.model.response.UserResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("users")
    fun fetchData(@Query("since") since: String = "0", @Query("per_page")
    page: String = "10"): Single<Response<ArrayList<GithubResponse>>>

    @GET("users")
    fun loadMoreUsers(@Query("q")keyWord: String, @Query("since") since: String, @Query("per_page")
    page: String = "10"): Single<Response<ArrayList<GithubResponse>>>

    @GET("search/users")
    fun searchUsers(@Query("q") keyWord: String, @Query("since") since: String = "0", @Query("per_page")
    page: String = "10"): Single<Response<SearchUserResponse>>

    @GET("users/{userID}")
    fun fetchUserData(@Path("userID") id: String): Single<Response<UserResponse>>
}