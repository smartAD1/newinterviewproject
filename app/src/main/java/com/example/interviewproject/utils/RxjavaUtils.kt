package com.example.interviewproject.utils

import androidx.lifecycle.MutableLiveData
import com.example.interviewproject.model.response.Status
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.core.SingleTransformer
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.schedulers.Schedulers
import retrofit2.Response


fun <T> applySingleSchedulersIO(): SingleTransformer<T, T> {
    return SingleTransformer { observable ->
        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
    }
}

fun <T> Single<Response<T>>.setResponse(liveData: MutableLiveData<Status>): Disposable {
    return subscribe({
        it.setData(liveData)
    }, {
        liveData.value = Status.Error("${it.message}")
    })
}