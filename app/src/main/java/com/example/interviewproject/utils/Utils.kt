package com.example.interviewproject.utils

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.example.interviewproject.model.response.Status
import retrofit2.Response

fun Context.toasts(message: String) = Toast.makeText(this, message, Toast.LENGTH_SHORT).show()


fun <T> Response<T>.setData(liveData: MutableLiveData<Status>) =
        when (this.code()) {
            200 -> {
                liveData.value = Status.Success(this.body())
            }
            else -> {
                liveData.value = Status.OnFailure
            }
        }
