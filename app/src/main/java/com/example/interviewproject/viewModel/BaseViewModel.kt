package com.example.interviewproject.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.interviewproject.utils.SearchBarStatus
import io.reactivex.rxjava3.disposables.CompositeDisposable

abstract class BaseViewModel : ViewModel() {
    val compositeDisposable = CompositeDisposable()
    val searchBarStatusLiveData: LiveData<Boolean> get() = _searchBarStatus
    private val _searchBarStatus = MutableLiveData<Boolean>()
    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

    fun setStatusBar() {
        _searchBarStatus.value = SearchBarStatus.searchBarStatus
    }
}