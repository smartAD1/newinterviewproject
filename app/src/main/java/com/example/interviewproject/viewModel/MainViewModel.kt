package com.example.interviewproject.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.interviewproject.model.repository.DataRepository
import com.example.interviewproject.model.response.Status
import com.example.interviewproject.utils.SearchBarStatus
import com.example.interviewproject.utils.applySingleSchedulersIO
import com.example.interviewproject.utils.setResponse
import io.reactivex.rxjava3.kotlin.addTo


class MainViewModel(private val dataRepository: DataRepository) : BaseViewModel() {

    val statusLiveData: LiveData<Status> get() = _statusData
    private val _statusData = MutableLiveData<Status>()

    var searchKeyWord = ""

    val searchUserDataStatus: LiveData<Status> get() = _searchUserDataStatus
    private val _searchUserDataStatus = MutableLiveData<Status>()

    fun fetchData() {
        dataRepository.fetchData()
                .compose(applySingleSchedulersIO())
                .setResponse(_statusData)
                .addTo(compositeDisposable)
    }

    fun searchUsers(keyWord: String) {
        searchKeyWord = keyWord
        dataRepository.searchUserData(keyWord)
                .compose(applySingleSchedulersIO())
                .setResponse(_searchUserDataStatus)
                .addTo(compositeDisposable)
    }


}