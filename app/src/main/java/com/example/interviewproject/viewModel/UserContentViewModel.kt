package com.example.interviewproject.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.interviewproject.model.repository.DataRepository
import com.example.interviewproject.model.response.Status
import com.example.interviewproject.utils.applySingleSchedulersIO
import com.example.interviewproject.utils.setResponse
import io.reactivex.rxjava3.kotlin.addTo

class UserContentViewModel(private val dataRepository: DataRepository) : BaseViewModel() {

    val statusLiveData: LiveData<Status> get() = _statusData
    private val _statusData = MutableLiveData<Status>()

    fun fetchUserData(id: String) {
        dataRepository.fetchUserData(id).compose(applySingleSchedulersIO())
                .setResponse(_statusData)
                .addTo(compositeDisposable)
    }
}