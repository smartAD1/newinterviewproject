package com.example.interviewproject

import androidx.multidex.MultiDexApplication
import com.example.interviewproject.model.repository.DataRepository
import com.example.interviewproject.viewModel.MainViewModel
import com.example.interviewproject.viewModel.UserContentViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module

class InterViewApplication : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()
        val viewModelModule = module {
            viewModel { MainViewModel(get()) }
            viewModel { UserContentViewModel(get()) }
        }
        val reportModule = module {
            single { DataRepository() }
        }
        startKoin {
            androidContext(this@InterViewApplication)
            modules(listOf(viewModelModule, reportModule))
        }
    }
}