package com.example.interviewproject.model.repository

import com.example.interviewproject.apiService.ApiService
import com.example.interviewproject.model.response.GithubResponse
import com.example.interviewproject.model.response.SearchUserResponse
import com.example.interviewproject.model.response.UserResponse
import com.example.interviewproject.utils.RetrofitManager
import io.reactivex.rxjava3.core.Single

import retrofit2.Response

class DataRepository : DataInterFace {

    private val retrofitManager: ApiService by lazy {
        RetrofitManager.getRetrofit()
    }

    private var everyPageList = 10

    override fun fetchData(): Single<Response<ArrayList<GithubResponse>>> =
            retrofitManager.fetchData(page = "$everyPageList")

    override fun searchUserData(keyWords: String): Single<Response<SearchUserResponse>> =
            retrofitManager.searchUsers(keyWords)

    override fun loadMoreUserData(keyWords: String): Single<Response<ArrayList<GithubResponse>>> {
        val (first, second) = getUserSize()
        return retrofitManager.loadMoreUsers(keyWord = keyWords, page = "$first", since = "$second")
    }


    override fun fetchUserData(id: String): Single<Response<UserResponse>> =
            retrofitManager.fetchUserData(id)


    private fun getUserSize(): Pair<Int, Int> {
        val last = everyPageList + 10
        val first = last - 9 ?: everyPageList++
        everyPageList = last
        return Pair(last, first)
    }
}