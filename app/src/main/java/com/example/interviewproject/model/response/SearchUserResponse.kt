package com.example.interviewproject.model.response


import com.google.gson.annotations.SerializedName

data class SearchUserResponse(
    @SerializedName("incomplete_results")
    var incompleteResults: Boolean = false,
    @SerializedName("items")
    var items: ArrayList<GithubResponse> = arrayListOf(),
    @SerializedName("total_count")
    var totalCount: Int = 0
)