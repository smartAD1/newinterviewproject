package com.example.interviewproject.model.response

sealed class Status {
    data class Success<T>(val data: T): Status()
    data class Error(val message: String): Status()
    object OnFailure: Status()
}